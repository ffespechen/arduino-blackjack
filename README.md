# Arduino BlackJack

Esta es una implementación sencilla del juego BlackJack o 21, utilizando un Arduino UNO.

Se juega contra "la casa", es decir, contra la selección de cartas que hace el programa para sí mismo.

Tiene las siguientes limitaciones:

Si obtienes dos Ases, pierdes... porque los Ases valen 11 puntos cada uno.
Puedes optar por solicitar solamente una carta extra, aparte de las que se reparten.
La casa no puede elegir más cartas que las dos primeras repartidas.

Los componentes del proyecto son:

Un Arduino UNO (en este caso)
Tres pulsadores: Amarillo para comenzar a jugar, Azul para "Pedir otra carta" y Rojo para "Plantarse"
Tres resistencias de 4.7 K, conectadas en cada pulsador en configuración PullDown
Un display LCD 2004 con adaptador I2C
y Cables varios

Las cartas que se eligen para la casa pueden verse en el Monitor Serial.