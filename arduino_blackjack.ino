//Librerías para el LCD
#include <Wire.h>
#include <LCD.h>
#include <LiquidCrystal_I2C.h>

//Constantes para el mazo de cartas
const int CANTIDAD_PALOS = 4;
const int CANTIDAD_FIGURAS = 13;
const int PALO_CORAZONES = 0;
const int PALO_PICAS = 1;
const int PALO_DIAMANTES = 2;
const int PALO_TREBOLES = 3;

//Variables globales
int mazoCartas[CANTIDAD_PALOS][CANTIDAD_FIGURAS];
int cartaSeleccionada[2];

//LCD
//Pin A4 SDA
//Pin A5 SCL
#define I2C_ADDR          0x27        //Define I2C Address where the PCF8574A is
#define BACKLIGHT_PIN      3
#define En_pin             2
#define Rw_pin             1
#define Rs_pin             0
#define D4_pin             4
#define D5_pin             5
#define D6_pin             6
#define D7_pin             7

//Initializar el LCD
LiquidCrystal_I2C      lcd(I2C_ADDR, En_pin,Rw_pin,Rs_pin,D4_pin,D5_pin,D6_pin,D7_pin);


//Pulsadores de comando
int pulsador_comenzar = 5;
int pulsador_pedir = 6;
int pulsador_plantarse = 7;

void setup() {

  //Definimos el LCD como 20x4 
  lcd.begin (20,4);
    
  //Encendemos la iluminación posterior
  lcd.setBacklightPin(BACKLIGHT_PIN,POSITIVE);
  lcd.setBacklight(HIGH);
    
  pinMode(pulsador_comenzar, INPUT);
  pinMode(pulsador_pedir, INPUT);
  pinMode(pulsador_plantarse, INPUT);

  randomSeed(analogRead(0));

  Serial.begin(9600);
  generarMazo();


}

//Función Principal
void loop() {

  lcd.setCursor(0,0);
  lcd.print("ARDUINO - BlackJack");

  lcd.setCursor(0,2);
  lcd.print("Presione Amarillo");
  lcd.setCursor(3,3);
  lcd.print("para Comenzar ->");

  if(digitalRead(pulsador_comenzar)==HIGH)
  {
    int jugador1_puntos = 0;
    int casa_puntos = 0;
    int pedir = 0;
    int plantar = 0;
    String carta;

    lcd.clear();

    lcd.setCursor(0,0);
    lcd.print("Cartas del Jugador");

    //Primera Carta
    repartirCarta();
    jugador1_puntos = jugador1_puntos + devolverValor(cartaSeleccionada[1]);
    carta = String(cartaSeleccionada[1])+" "+devolverPalo(cartaSeleccionada[0]);
    lcd.setCursor(0,1);
    lcd.print(carta);

    //Segunda Carta
    repartirCarta();
    jugador1_puntos = jugador1_puntos + devolverValor(cartaSeleccionada[1]);
    carta = String(cartaSeleccionada[1])+" "+devolverPalo(cartaSeleccionada[0]);
    lcd.setCursor(10,1);
    lcd.print(carta);    


    //Puntos de la casa
    repartirCarta();
    casa_puntos = casa_puntos + devolverValor(cartaSeleccionada[1]);
    Serial.println(String(cartaSeleccionada[1])+" "+devolverPalo(cartaSeleccionada[0]));
    repartirCarta();
    casa_puntos = casa_puntos + devolverValor(cartaSeleccionada[1]);
    Serial.println(String(cartaSeleccionada[1])+" "+devolverPalo(cartaSeleccionada[0])); 

    
    lcd.setCursor(0,2);
    lcd.print("Jugad:"+String(jugador1_puntos));
    lcd.setCursor(10,2);
    lcd.print("Casa:"+String(casa_puntos));

    if(casa_puntos>21)
    {
      lcd.setCursor(0,3);
      lcd.print("GANA EL JUGADOR     ");
      delay(10000);
    }
    else
    {
      pedirPlantar();

      while(pedir==LOW && plantar==LOW)
      {
        pedir = digitalRead(pulsador_pedir);
        plantar = digitalRead(pulsador_plantarse);
        delay(100);
      }

      if(pedir==HIGH)
      {
        repartirCarta();
        jugador1_puntos = jugador1_puntos + devolverValor(cartaSeleccionada[1]);
        carta = String(cartaSeleccionada[1])+" "+devolverPalo(cartaSeleccionada[0]);
        lcd.setCursor(0,2);
        lcd.print(carta);
      }

      if(jugador1_puntos<21 && jugador1_puntos>=casa_puntos)
      {
        lcd.setCursor(0,3);
        lcd.print("GANA EL JUGADOR     ");
      }
      else
      {
        lcd.setCursor(0,3);
        lcd.print("GANA LA CASA     ");
      }
    }

    delay(7000);
    lcd.clear();
    
  }
  
}


//Genera el mazo de cartas
void generarMazo()
{
  for(int i=0; i<CANTIDAD_PALOS; i++)
  {
    for(int j=0; j<CANTIDAD_FIGURAS; j++)
    {
      mazoCartas[i][j] = true;
    }
  }
}

//Devuelve una carta al azar que previamente no se haya seleccionado
void repartirCarta()
{
  bool seleccion = true;
  int indice_palos;
  int indice_figuras;

  while(seleccion)
  {
    indice_palos = random()%(CANTIDAD_PALOS);
    indice_figuras = random()%(CANTIDAD_FIGURAS);

    if(mazoCartas[indice_palos][indice_figuras]==true)
    {
      cartaSeleccionada[0] = indice_palos;
      cartaSeleccionada[1] = (indice_figuras+1);
      mazoCartas[indice_palos][indice_figuras]=false;
      seleccion=false;
    }
  }
  
}

//Devuelve un String con la descripción del palo de la carta seleccionada
String devolverPalo(int palo)
{
  switch(palo)
  {
    case PALO_CORAZONES:
      return "CORAZ";
      break;
    case PALO_PICAS:
      return "PICAS";
      break;
    case PALO_DIAMANTES:
      return "DIAMA";
      break;
    case PALO_TREBOLES:
      return "TREBL";
      break;
    default:
      return "NO/VAL";
      break;
  }
}


//Devuelve el valor relativo de la carta elegida
//AS = 11
//FIGURAS = 10
int devolverValor(int figura)
{
  switch(figura)
  {
    case 1:
      return 11;
      break;
    case 11:
      return 10;
      break;
    case 12:
      return 10;
      break;
    case 13:
      return 10;
      break;
    default:
      return figura;
      break;
  }
}

void pedirPlantar()
{
  lcd.setCursor(0,3);
  lcd.print("+:AZUL / No:ROJO");
}

